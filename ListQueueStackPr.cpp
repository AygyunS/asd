#include <iostream>
#include <list>
#include <queue>
#include <stack>
#include <stdlib.h>

using namespace std;

int IsPrime(int n){
    for(int i=2;i*i<=n;i++)
        if(n%i==0)
         return 0;
    return 1;
}

int main()
{
    list <int> l1;
    list <int> l2;
    list <int> l3;
    stack <int> st;
    queue<int> q;
    list<int>::iterator it;
    /*
    for(int i = 1;i<=9;i++){
        int random =1+ rand() % 100;
        l1.push_back(random);
    }
    */
    /*
    //Vavejdame stoinostite na spisyka
    int x;
    for(int i = 1;i<=9;i++){
        cin >> x;
        l1.push_back(x);
    }
    */

    //gotow spisyk...
    l1.push_back(13);    //push back kym spisyka
    l1.push_back(-5);
    l1.push_back(4);
    l1.push_back(2);
    l1.push_back(-7);
    l1.push_back(8);
    l1.push_back(15);
    l1.push_back(-3);
    l1.push_back(6);

    //otpechatwane na pyrwichniq spisyk
    for(it = l1.begin();it !=l1.end();it++){
        cout << *it << " ";
    }
    cout << endl;
    //krai na otpechatwaneto


    //zadachi sys stack i quene i list
    it = l1.begin();
    while(it!=l1.end()){
        if(*it<0){
            cout << *it << " ";
        }
        if(*it>0){
            q.push(*it);
            it=l1.erase(it);
        }
        else it++;

    }
    cout << endl;
    while(!q.empty())
    {
        int x = q.front();
        cout << x << " ";
        q.pop();
        if(IsPrime(x)){
            st.push(x);
        }
    }
    cout << endl;
    while(!st.empty()){
        int w = st.top();
        cout << w << " ";
        st.pop();
    }

    return 0;
}
