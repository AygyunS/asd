#include <iostream>
#include <stack>
#include <queue>
#include <deque>

using namespace std;

int main()
{
    stack <int> st;
    int k;
    cin >> k;
    for (int i=k; i>0; i--) {
        st.push(i);
    }
    while(!st.empty()){
        int x = st.top();
        st.pop();
        cout << x << " ";
        for(int i = x-1; i>0; i--){
            st.push(i);
        }

    }
    cout << endl;

    return 0;
}
