//zadacha sys 3 spisyka
//3 5 9 8 7 6 17 4 15 20
//7 19 12 6 1 5 2 22 30 13
//tam kydeto se zasichat edni i syshti chisla se premahwat ot 3-tiq spisyk
#include <iostream>
#include <list>
#include <stdlib.h>

using namespace std;

int Search(list <int> lst, int key){
    list<int>::iterator it;
    for(it=lst.begin(); it !=lst.end();it++){
        if(*it == key){
            return 1;
        }
    }
    return 0;
}

int main()
{
    list <int> l1;
    list <int> l2;
    list <int> l3;
    list<int>::iterator it;
    list<int>::iterator it2;

    /*
    for(int i = 1;i<=9;i++){
        int random =1+ rand() % 100;
        l1.push_back(random);
    }
    */
    /*
    //Vavejdame stoinostite na spisyka
    int x;
    for(int i = 1;i<=9;i++){
        cin >> x;
        l1.push_back(x);
    }
    */

    //gotow spisyk...
    l1.push_back(3);    //push back kym spisyka
    l1.push_back(5);
    l1.push_back(9);
    l1.push_back(8);
    l1.push_back(7);
    l1.push_back(6);
    l1.push_back(17);
    l1.push_back(4);
    l1.push_back(15);
    l1.push_back(20);

    l2.push_back(7);    //push back kym wtoriq spisyk
    l2.push_back(19);
    l2.push_back(12);
    l2.push_back(6);
    l2.push_back(1);
    l2.push_back(5);
    l2.push_back(2);
    l2.push_back(22);
    l2.push_back(30);
    l2.push_back(13);


    //otpechatwane na pyrwichniq spisyk
    for(it = l1.begin();it !=l1.end();it++){
        cout << *it << " ";
    }
    cout << endl;
    for(it = l2.begin();it !=l2.end();it++){
        cout << *it << " ";
    }
    cout << endl;
    //krai na otpechatwaneto


    //zadacha sys 3 spisyka
	/*
	//pyrwo reshenie
    for(it = l1.begin();it !=l1.end();it++){
        for(it2 = l2.begin();it2 !=l2.end();it2++){
            if(*it==*it2){
                l2.erase(it2--);
            }


        }
    }
    l3.insert(l3.begin(),l1.begin(),l1.end());
    l3.insert(l3.begin(),l2.begin(),l2.end());

    for(it = l3.begin();it !=l3.end();it++){
        cout << *it << " ";
    }
	*/

	//wtoro reshenie
	l3.insert(l3.begin(),l1.begin(),l1.end());
	for(it=l2.begin();it!=l2.end();it++){
        if(Search(l3,*it)==0){
            l3.push_back(*it);
        }
    }

    //otpechatwane na krainiq 3-ti spisyk
    for(it = l3.begin();it !=l3.end();it++){
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}
