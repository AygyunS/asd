#include <iostream>
#include <stack>
#include <string>

using namespace std;

bool operation(char b)
{
    return b=='+' || b=='-' || b=='*' || b=='/' ;
}

bool priority(char a, char b)
{
    if(a=='('){
        return true;
    }
    if(a=='+' || a=='-'){
        return true;
    }
    if(b=='+' || b=='-'){
        return false;
    }
    return true;
}
int main()
{
    string a;
    string res;
    stack<char> st;
    cin>>a;
    for(int i=0; i<a.size(); i++)
    {
        if(a[i]!='(' && a[i]!=')' && operation(a[i])==false){
            res=res+a[i];
        }
        if(a[i]=='('){
            st.push(a[i]) ;
        }
        if(a[i]==')'){
            while(st.top()!='('){
                res+=st.top();
                st.pop();
            }
            st.pop();
        }
        if(operation(a[i])==true){
            if(st.empty() || (st.empty()==false && priority(st.top(), a[i])) ){
                st.push(a[i]);
            }
            else{
                while(st.empty()==false && priority(st.top(),a[i])==false )
                {
                    res+=st.top();
                    st.pop();
                }
                st.push(a[i]) ;
            }
        }
    }
    while(st.empty()==false){
        res+=st.top();
        st.pop();
    }
    cout<<res;

    return 0;
}
